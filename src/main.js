import Vue from "vue"
import page from "page"
import routes from './js/routes.js'
import config from "./js/config.js";
import axios from "axios";

function networkInfo() {
    let Connection = navigator.connection;
    var networkState = navigator.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';
    // alert('Connection type: ' + states[networkState]);

    // if(states[Connection.NONE]) {
        // alert('there is no internet')
    // }
}
networkInfo();

const app = new Vue({
    el: '#app',
    data: {
        ViewComponent: { render: h => h('div', 'loading...') },
        params: null,
        bddlocale: null
    },
    render (h) { return h(this.ViewComponent) }
})

let competitions;
let dossards;
let epreuves;
let db;

axios.get('https://'+config.host+'/listedossards')
    .then(response => {
        // handle success
        dossards = response.data;
        if (competitions !== 'undefined' && epreuves !== 'undefined' && dossards !== 'undefined' && typeof db === 'undefined') {
            bddlocale();
        }
    })
    .catch(error => {
        console.log(error);
        // handle error
    });

axios.get('https://'+config.host+'/listescompetitions')
    .then(response => {
        // handle success
        competitions = response.data;
        if (competitions !== 'undefined' && epreuves !== 'undefined' && dossards !== 'undefined' && typeof db === 'undefined') {
            bddlocale();
        }
    })
    .catch(error => {
        console.log(error);
        // handle error
    });

axios.get('https://'+config.host+'/listeepreuves')
    .then(response => {
        // handle success
        epreuves = response.data;
        if (competitions !== 'undefined' && epreuves !== 'undefined' && dossards !== 'undefined' && typeof db === 'undefined') {
            bddlocale();
        }
    })
    .catch(error => {
        console.log(error);
        // handle error
    });

function bddlocale() {
    db = openDatabase('equitrec', '1.0', 'Test DB', 2 * 1024 * 1024);
    db.transaction(function (tx) {
        tx.executeSql("DROP TABLE IF EXISTS competitions");
        tx.executeSql('CREATE TABLE competitions (id serial, nom varchar(255),id_dossard int NOT NULL,id_epreuve int NOT NULL,id_user int NOT NULL,criteres varchar(255), note_final int,date date,PRIMARY KEY (id, id_dossard, id_epreuve, id_user))');
        for (var i = 0; i < competitions.length; i++) {
            let toto = "INSERT INTO competitions(id,nom,id_dossard,id_epreuve,critères,note_final,id_user,observations,date) VALUES(" +
                competitions[i].id +
                ", '" +
                competitions[i].nom +
                "', '" +
                competitions[i].id_dossard +
                "', '" +
                competitions[i].id_epreuve +
                "', '" +
                competitions[i].critères +
                "', '" +
                competitions[i].note_final +
                "', '" +
                competitions[i].id_user +
                "', '" +
                competitions[i].observations +
                "', '" +
                competitions[i].date +
                "')";
            tx.executeSql(toto);
        }
        tx.executeSql("DROP TABLE IF EXISTS dossards");
        tx.executeSql("CREATE TABLE dossards (numero serial, nom varchar(255),prenom varchar(255), PRIMARY KEY (numero))");
        for (var i = 0; i < dossards.length; i++) {
            let titi = "INSERT INTO dossards(numero,nom,prenom) VALUES(" +
                dossards[i].numero +
                ", '" +
                dossards[i].nom +
                "', '" +
                dossards[i].prenom +
                "')";
            tx.executeSql(titi);

        }
        tx.executeSql("DROP TABLE IF EXISTS epreuves");
        tx.executeSql("CREATE TABLE epreuves (id serial, nom varchar(255), PRIMARY KEY (id))");
        for (var i = 0; i < epreuves.length; i++) {
            let titi = "INSERT INTO epreuves(id,nom) VALUES(" +
                epreuves[i].id +
                ", '" +
                epreuves[i].nom +
                "')";
            tx.executeSql(titi);
        }
    });

    if (db) {
        app.bddlocale = db;
        app.ViewComponent.bddLoaded();
    }
}

Object.keys(routes).forEach(route => {
    const Component = require('./vues/views/' + routes[route] + '.vue').default
    page(route, function (ctx) {
        app.params = ctx.params;
        app.ViewComponent = Component;
    })
})

page('*', () => app.ViewComponent = require('./vues/views/NotFound.vue').default)
page()

