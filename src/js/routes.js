export default {
    '/': 'Login',
    '/accueil' : 'Accueil',
    '/dossardlist/:epreuve': 'DossardList',
    '/epreuvelist': 'EpreuveList',
    '/notes/:epreuve': 'Notes',
    '/notes/:epreuve/:dossard': 'Notes'
}